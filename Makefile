###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 07d - Animalfarm0 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Animalfarm1
###
### @author Ashten Akemoto <aakemoto@hawaii.edu>
### @date 02-17-2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC = gcc
CFLAGS = -g -Wall -Wextra
TARGET = animalFarm

all: $(TARGET)

addCats.o: addCats.c addCats.h
	$(CC) $(CFLAGS) -c addCats.c

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c

reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c

animalFarm.o: animalFarm.c config.h
	$(CC) $(CFLAGS) -c animalFarm.c

animalFarm: animalFarm.o reportCats.o catDatabase.o addCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $(TARGET) animalFarm.o reportCats.o catDatabase.o addCats.o updateCats.o deleteCats.o

clean:
	rm -f $(TARGET) *.o

test: animalFarm
	./animalFarm