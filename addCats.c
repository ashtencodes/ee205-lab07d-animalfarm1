#include <assert.h>
#include <string.h>
#include <stdio.h>
#include "config.h"
#include "addCats.h"

bool addCat( char newName[], enum Gender addGender, enum Breed addBreed, bool isFixed, float weight, enum Color collarColor1, enum Color collarColor2, unsigned long long license ){
    if(isValidName(newName) && isValidWeight(weight)){

        strcpy(catArray[currentCatNum].name, newName);
        catArray[currentCatNum].breed = addBreed;
        catArray[currentCatNum].isFixed = isFixed;
        catArray[currentCatNum].weight = weight;
        catArray[currentCatNum].gender = addGender;
        catArray[currentCatNum].collarColor1 = collarColor1;
        catArray[currentCatNum].collarColor2 = collarColor2;
        catArray[currentCatNum].license = license;

        currentCatNum += 1;
        return true;
    } else {
        return false;
    }
 };

