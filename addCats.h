#pragma once
#include "catDatabase.h"

bool addCat( char newName[], enum Gender addGender, enum Breed addBreed, bool isFixed, float weight, enum Color collarColor1, enum Color collarColor2, unsigned long long license );