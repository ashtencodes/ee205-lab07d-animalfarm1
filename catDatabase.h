#pragma once
#include <stdbool.h>

#define MAX_NUM_CATS 30
#define NAME_LEN_MAX 50

extern int currentCatNum;

enum Gender { UNKNOWN_GENDER, MALE, FEMALE };
enum Breed { UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHINX };
enum Color { BLACK, WHITE, RED, BLUE, GREEN, PINK };

struct Cat {
    enum Gender gender;
    enum Breed breed;
    enum Color collarColor1;
    enum Color collarColor2;
    bool isFixed;
    float weight;
    unsigned long long license;
    char name[NAME_LEN_MAX];
};

extern struct Cat catArray[];

extern bool isValidIndex( const int index );
extern bool isValidName( const char checkName[] );
extern bool isValidWeight( const float checkWeight ) ;
