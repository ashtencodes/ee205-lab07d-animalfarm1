#pragma once
#include "catDatabase.h"

bool updateCatName(int index, char name[]);

bool fixCat(int index);

bool updateCatWeight (int index, float newWeight);

bool updateCollar1 ( int index, enum Color newColor );
bool updateCollar2 ( int index, enum Color newColor );
bool updateLicense ( int index, unsigned long long newLicense );
